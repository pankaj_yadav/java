package trendnxt_core_java_l1_top_gear_assignments;


public class T4Q3 {

    public static void main(String[] args) {
	T4Q3EmployeeDB empDb = new T4Q3EmployeeDB();

	T4Q3Employee emp1 = new T4Q3Employee(101, "Bob", "bob@w3epic.com", 'M', 25000);
	T4Q3Employee emp2 = new T4Q3Employee(102, "Alice", "alice@w3epic.com", 'F', 30000);
	T4Q3Employee emp3 = new T4Q3Employee(103, "John", "john@w3epic.com", 'M', 20000);
	T4Q3Employee emp4 = new T4Q3Employee(104, "Ram", "ram@w3epic.com", 'M', 50000);

	empDb.addEmployee(emp1);
	empDb.addEmployee(emp2);
	empDb.addEmployee(emp3);
	empDb.addEmployee(emp4);

	for (T4Q3Employee emp : empDb.listAll())
	    System.out.println(emp.GetEmployeeDetails());

	System.out.println();
	empDb.deleteEmployee(102);

	for (T4Q3Employee emp : empDb.listAll())
	    System.out.println(emp.GetEmployeeDetails());

	System.out.println();

	System.out.println(empDb.showPaySlip(103));

    }

}
