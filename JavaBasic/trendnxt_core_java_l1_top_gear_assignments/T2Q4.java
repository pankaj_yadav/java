package trendnxt_core_java_l1_top_gear_assignments;

class Payment{
	double amount;
	
	public double getAmount() {
		return(this.amount);
	}
	public void setAmount(double amount) {
		this.amount=amount;
	}
	public String paymentDetails() {
		return("The amount of payment is "+getAmount());
	}
}

class CashPayment extends Payment{
	public CashPayment(double amount) {
		super.setAmount(amount);
	}
	public String displayCash() {
		return(super.paymentDetails()+" through Cash Payment");
	}
}
class CreditCardPayment extends Payment{
	String name;
	String date;
	String cardNo;
	public CreditCardPayment(double amount, String name, String date, String cardNo) {
		this.name=name;
		this.date=date;
		this.cardNo=cardNo;
		super.setAmount(amount);
	}
	public String displayCreditCard() {
		return(super.paymentDetails()+" through Credit Card\nCredit Card Details :: \n"
		+ "Card Holder Name : "+this.name+"\nExpiry Date : "+this.date
		+"\nCard No : "+this.cardNo);
	}
}
public class T2Q4 {
	public static void main(String[] args) {
		CashPayment cash=new CashPayment(2300);
		CreditCardPayment card = new CreditCardPayment(23000,"Pankaj","21-04-2025","5459649200060300");
		System.out.println(cash.displayCash());
		System.out.print(card.displayCreditCard());
	}

}
