package trendnxt_core_java_l1_top_gear_assignments;
import java.util.Scanner;

public class T1Q5 {

	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		
		System.out.print("Enter any number of 4 digits : ");
		String n=in.nextLine();

		int count= Integer.parseInt(n.substring(0,1))+ Integer.parseInt(n.substring(1,2))+ Integer.parseInt(n.substring(2,3))+ Integer.parseInt(n.substring(3,4));
		
		System.out.println("The sum of all the digits entered is "+count);
	}

}
