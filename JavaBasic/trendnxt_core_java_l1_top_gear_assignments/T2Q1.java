package trendnxt_core_java_l1_top_gear_assignments;

public class T2Q1 {

	int isbn;
	String title;
	String author;
	float price;
	
	public T2Q1(int isbn, String title, String author, float price) {
		this.isbn=isbn;
		this.title=title;
		this.author=author;
		this.price=price;
	}

	public void displayDetails() {
		System.out.println("Book Details :: ");
		System.out.println("S. No. : "+this.isbn);
		System.out.println("Title  : "+this.title);
		System.out.println("Author : "+this.author);
		System.out.println("Price  : "+this.price);
	}
	
	public float discountedPrice(int disPercent) {
		return this.price- (this.price*disPercent)/100;
		
	}
	
	public static void main(String[] args) {
		
		T2Q1 book1=new T2Q1(002,"The Secret", "Rhonda Byrne", 250);
		book1.displayDetails();
		System.out.print("\nDiscounted Price : "+book1.discountedPrice(25));
	}

}
