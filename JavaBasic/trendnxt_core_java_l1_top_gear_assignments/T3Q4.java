package trendnxt_core_java_l1_top_gear_assignments;
import java.util.*;
public class T3Q4 {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		
		String word=in.nextLine().toUpperCase();
		int flag=0;
		for(int i=0;i<word.length()/2;i++) {
			if(word.charAt(i)!=word.charAt(word.length()-i-1)) {
				flag=1;
				break;
			}
		}
		if(flag==0) {
			System.out.println("String is Palindrome");
		}
		else {
			System.out.println("String is not a Palindrome");
		}
	}

}

