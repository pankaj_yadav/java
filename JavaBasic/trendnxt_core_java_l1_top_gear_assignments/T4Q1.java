package trendnxt_core_java_l1_top_gear_assignments;
	class RandomNo implements Runnable{
		private Thread t;
		static int num;
		public void run() {
			try {
				for(int i=0;i<5;i++) {
					num=(int)(Math.random()*10);
					System.out.println("Number : "+num);
					Thread.sleep(10000);
				}
			}catch(InterruptedException e) {
				System.out.println(t.getName()+"Interrupted");
			}
		}
	}
	class Factorial implements Runnable{
		private Thread t;
		private int factorialNum;
		private int fact;
		public int factorial(int num) {
			fact=1;
			for(int i=1;i<=RandomNo.num;i++){    
			      fact=fact*i;    
			 }
			return fact;
		}
		
		public void run() {
			try {
				for(int i=0;i<5;i++) {
					factorialNum =factorial(RandomNo.num);
					System.out.println("Factorial of "+RandomNo.num+" = "+ factorialNum);
					Thread.sleep(10000);
				}
			}catch(InterruptedException e) {
				System.out.println(t.getName()+"Interrupted");
			}
		}
	}

		public class T4Q1 {

		   public static void main(String args[]) {
			  Thread t1 = new Thread(new RandomNo());
		      t1.start();
		      
		      Thread t2 = new Thread(new Factorial());
		      t2.start();
		      try {
		    	  t1.join();
		    	  t2.join();
		      }
		      catch(Exception ex){
		    	  
		      }
		   }   
		}