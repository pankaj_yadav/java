package trendnxt_core_java_l1_top_gear_assignments;
import Automobile.TwoWheeler.Hero;
import Automobile.TwoWheeler.Honda;

public class T5Q3 {

    public static void main(String[] args) {
	Hero hero = new Hero("Hero Moto","RJ12532","Perry",90);
	System.out.println(hero.modelName());
	System.out.println(hero.registrationNumber());
	System.out.println(hero.ownerName());
	System.out.println(hero.speed());
	hero.radio();
	Honda honda = new Honda("Honda Motor","PB8686","Bunny",60);
	System.out.println(honda.modelName());
	System.out.println(honda.registrationNumber());
	System.out.println(honda.ownerName());
	System.out.println(honda.speed());
	honda.cdplayer();
    }

}
