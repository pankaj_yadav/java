package trendnxt_core_java_l1_top_gear_assignments;
import java.util.Scanner;
import java.util.Arrays;
import java.util.*;

public class T1Q6 {

	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		
		System.out.print("Enter the values of array (space seperated) : ");
		String s=in.nextLine();
		
		String arr[]=s.split(" ");
		
		int ar[]=new int[arr.length];
		
		for(int i=0;i<arr.length;i++) {
			ar[i]=Integer.parseInt(arr[i]);
		}
		
		int max=ar[0];
		for(int i=0;i<ar.length;i++) {
			if(max<ar[i])
				max=ar[i];
		}
		
		System.out.print("Maximum of the array : "+max);
	}

}
