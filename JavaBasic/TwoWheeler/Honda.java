package Automobile.TwoWheeler;

import Automobile.Vehicle;

public class Honda extends Vehicle {
    public int speed;
    String modelName;
    String registrationNumber;
    String ownerName;

    public Honda(String modelName, String registrationNumber, String ownerName, int speed) {
	super();
	this.modelName = modelName;
	this.registrationNumber = registrationNumber;
	this.ownerName = ownerName;
	this.speed = speed;
    }

    @Override
    public String modelName() {
	return "Model Name : " + modelName;
    }

    @Override
    public String registrationNumber() {
	return "Registration No : " + registrationNumber;
    }

    @Override
    public String ownerName() {
	return "Owner Name : " + ownerName;
    }

    public int speed() {
	System.out.print("The current speed of the vehicle : ");
	return speed;
    }

    public void cdplayer() {
	System.out.println("Controlling the cd player device which is available in the car");
    }
}
