package Automobile.TwoWheeler;

import Automobile.Vehicle;

public class Hero extends Vehicle {
    public int speed;
    String modelName;
    String registrationNumber;
    String ownerName;

    public Hero(String modelName, String registrationNumber, String ownerName, int speed) {
	super();
	this.modelName = modelName;
	this.registrationNumber = registrationNumber;
	this.ownerName = ownerName;
	this.speed = speed;
    }

    @Override
    public String modelName() {
	return "Model Name : " + modelName;
    }

    @Override
    public String registrationNumber() {
	return "Registration No : " + registrationNumber;
    }

    @Override
    public String ownerName() {
	return "Owner Name : " + ownerName;
    }

    public int speed() {
	System.out.print("Current speed of the vehicle : ");
	return speed;
    }

    public void radio() {
	System.out.println("Accessing the radio device");
    }
}
