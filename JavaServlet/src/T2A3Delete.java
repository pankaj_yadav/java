
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class T2A3Delete
 */
@WebServlet("/T2A3Delete")
public class T2A3Delete extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system", "orcl");
	    System.out.println("Connection Establsihed: " + con);

	    Statement stmt = con.createStatement();
	    ResultSet rs = stmt.executeQuery("SELECT TRAININGNAME FROM TRAINING");

	    out.println("<html><body><table border='1'><tr><td>Training</td></tr>");

	    while (rs.next()) {
		out.println("<tr><td>" + rs.getString(1) + "</td> </tr>");

	    }
	    out.println("</table></body></html>");

	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	}
	RequestDispatcher rs = request.getRequestDispatcher("DeleteTraining.html");
	rs.include(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	PrintWriter out = response.getWriter();
	String trainingName = request.getParameter("training_name");
	
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system", "orcl");
	    System.out.println("Connection Establsihed: " + con);

	    Statement stmt = con.createStatement();
	    int valid = stmt
		    .executeUpdate("SELECT * FROM TRAINING WHERE TRAININGNAME = '" + trainingName + "'");

	    if (valid >= 1) {
		int deleted = stmt.executeUpdate("DELETE FROM TRAINING WHERE TRAININGNAME = '" + trainingName + "'");
		if (deleted >= 1) {
		    out.print("Training Deleted Successfully!");
		} else {
		    out.print("Unable to delete Training! Please,try again.");
		    RequestDispatcher rs = request.getRequestDispatcher("DeleteTraining.html");
		    rs.include(request, response);
		}
	    } else {
		out.print("Training not found!");
	    }
	    doGet(request, response);

	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	}

    }
}
