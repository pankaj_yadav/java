
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class T2A1 extends HttpServlet {
 
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String userName = request.getParameter("uname");
        String pass = request.getParameter("pass");
        
        if(!userName.isEmpty() && !pass.isEmpty() )
        {
            out.println("Welcome User");
        }
        else
        {
           out.println("Username or Password incorrect");
           RequestDispatcher rs = request.getRequestDispatcher("T2A1.html");
           rs.include(request, response);
        }
    }  
    
}