
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/T3A2")
public class T3A2 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public T3A2() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String userType = "";
	String userName = request.getParameter("uname");
	String password = request.getParameter("pass");
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();
	try {
	    Class.forName(getServletContext().getInitParameter("DB_CONN"));
	    Connection con = DriverManager.getConnection(getServletConfig().getInitParameter("DB_URL"),
		    getServletConfig().getInitParameter("DB_USER"), getServletConfig().getInitParameter("DB_PSWD"));
	    System.out.println("Connection Established: " + con);

	    Statement stmt = con.createStatement();
	    int valid = stmt.executeUpdate(
		    "SELECT USERNAME FROM USERS WHERE USERNAME='" + userName + "' AND PASSWORD='" + password + "'");
	    if (valid >= 1) {
		out.println("Welcome "+userName);
		
	    } else {
		out.println("Invalid Credentials! Try Again/n");
		RequestDispatcher rs = request.getRequestDispatcher("connectionInfo.html");
		rs.include(request, response);
	    }
	    con.close();
	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	}

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	doGet(request, response);
    }

}
