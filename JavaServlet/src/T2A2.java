
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class T2A2
 */
@WebServlet("/T2A2")
public class T2A2 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public T2A2() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();

	String userName = request.getParameter("uname");
	String pass = request.getParameter("pass");

	if (!userName.isEmpty() && !pass.isEmpty()) {
	    String userType = checkUser(userName, pass);

	    if (userType.equalsIgnoreCase("A")) {
		RequestDispatcher rs = request.getRequestDispatcher("AdminHomePage.html");
		rs.include(request, response);
	    } else if (userType.equalsIgnoreCase("E")) {
		RequestDispatcher rs = request.getRequestDispatcher("EmployeeHomePage.html");
		rs.include(request, response);
	    } else {
		out.println("Username or Password Incorrect");
		RequestDispatcher rs = request.getRequestDispatcher("T2A2.html");
		rs.include(request, response);
	    }
	} else {
	    out.println("Username or Password Empty");
	    RequestDispatcher rs = request.getRequestDispatcher("T2A2.html");
	    rs.include(request, response);
	}
    }

    protected String checkUser(String userName, String password) {
	String userType = "";
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system", "orcl");
	    System.out.println("Connection Established: " + con);

	    Statement stmt = con.createStatement();
	    int valid = stmt.executeUpdate("SELECT USERNAME FROM USERS WHERE USERNAME='" + userName
		    + "' AND PASSWORD='" + password + "'");
	    if (valid >= 1) {
		ResultSet rs = stmt.executeQuery("SELECT USERTYPE FROM USERS WHERE USERNAME='" + userName + "'");
		while (rs.next()) {
		    userType += rs.getString("USERTYPE");
		}
	    } else {
		userType += "invalid";
	    }
	    con.close();
	    return userType;
	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	    return null;
	}
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
