
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class T2Q3
 */
@WebServlet("/T2Q3")
public class T2A3Add extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public T2A3Add() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();
	
	int trainingId = Integer.parseInt(request.getParameter("training_id"));
	String trainingName = request.getParameter("training_name");
	String startDate = request.getParameter("start_date");
	String endDate =request.getParameter("end_date");
	String trainingMode = request.getParameter("training_mode");
	int businessUnit = Integer.parseInt(request.getParameter("business_unit"));
	int contactPersonId = Integer.parseInt(request.getParameter("contact_person_id"));
	out.print(addUser(trainingId, trainingName, startDate, endDate, trainingMode, businessUnit, contactPersonId));
	RequestDispatcher rs = request.getRequestDispatcher("AddTraining.html");
        rs.include(request, response);
    }

    protected String addUser(int trainingId, String trainingName, String startDate, String endDate, String trainingMode,
	    int businessUnit, int contactPersonId) {
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system", "orcl");
	    System.out.println("Connection Established: " + con);

	    Statement stmt = con.createStatement();
	    int valid = stmt.executeUpdate(
		    "INSERT INTO TRAINING VALUES(" + trainingId + ",'" + trainingName + "','" + startDate + "','"
			    + endDate + "','" + trainingMode + "'," + businessUnit + "," + contactPersonId + ")");
	    con.close();
	    if(valid >=1) {
		return "Training Added Successfully!";
	    }
	    else {
		return "Please, Provide Valid Inputs!";
	    }
	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	    return null;
	}
    }
}
