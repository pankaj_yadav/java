
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/T4A4")
public class T4A4 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public T4A4() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String userName = request.getParameter("uname");
	String password = request.getParameter("pass");
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system", "orcl");
	    System.out.println("Connection Established: " + con);

	    Statement stmt = con.createStatement();
	    int valid = stmt.executeUpdate(
		    "SELECT USERNAME FROM USERS WHERE USERNAME='" + userName + "' AND PASSWORD='" + password + "'");

	    if (valid >= 1) {
		HttpSession session=request.getSession();  
	        session.setAttribute("uname",userName); 
		RequestDispatcher rs = request.getRequestDispatcher("salary.html");
		rs.include(request, response);
	    } else {
		out.println("Log In Here");
		RequestDispatcher rs = request.getRequestDispatcher("T4A4login.html");
		rs.include(request, response);
	    }
	    con.close();

	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	}
	 
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
