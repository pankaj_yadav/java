
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/T1A2")
public class T1A2 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public T1A2() {
	super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
	String name = request.getParameter("uname");

	Date date = new Date();
	String temp = "", hour = date.toString().substring(11, 13);
	if (Integer.parseInt(hour) >= 0 && Integer.parseInt(hour) < 12) {
	    temp += "Morning";
	} else {
	    if (Integer.parseInt(hour) >= 12 && Integer.parseInt(hour) < 18) {
		temp += "Afternoon";
	    } else {
		temp += "Evening";
	    }

	}
	out.println("Good " + temp + ", " + name);

    }
}
