
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/T4A1")
public class T4A1 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public T4A1() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String userName = request.getParameter("uname");
	String password = request.getParameter("pass");
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system", "orcl");
	    System.out.println("Connection Established: " + con);

	    Cookie ck[] = request.getCookies();

	    if (ck == null) {
		Statement stmt = con.createStatement();
		int valid = stmt.executeUpdate(
			"SELECT USERNAME FROM USERS WHERE USERNAME='" + userName + "' AND PASSWORD='" + password + "'");

		if (valid >= 1) {
		    out.println("Welcome " + userName);
		    Cookie ck1 = new Cookie("user_name", userName);
		    response.addCookie(ck1);
		    Cookie ck2 = new Cookie("password", password);
		    response.addCookie(ck2);

		} else {
		    out.println("Log In Here");
		    RequestDispatcher rs = request.getRequestDispatcher("logMeIn.html");
		    rs.include(request, response);
		}
		con.close();
	    } else {
		out.println("Welcome Back " + ck[0].getValue());
		out.println("(Logged in with Cookies)");
		for (int i = 0; i < ck.length; i++) {

		    Cookie cookie = ck[i];
		    ck[i].setValue(null);
		    ck[i].setMaxAge(0);
		    response.addCookie(cookie);
		}
	    }
	} catch (Exception e) {
	    System.out.println("Connection Not Established: " + e);
	}
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	doGet(request, response);
    }

}
