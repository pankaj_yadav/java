

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class T3A1
 */
@WebServlet("/T3A1")
public class T3A1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public T3A1() {
        super();
        
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int key=Integer.parseInt(request.getParameter("key"));
		if(key<10) {
		    RequestDispatcher rd = request.getRequestDispatcher("page1.html");
		    rd.include(request, response);
		}
		else if(key<99){
		    RequestDispatcher rd = request.getRequestDispatcher("page2.html");
		    rd.include(request, response);
		}
		    
		else{
		    RequestDispatcher rd = request.getRequestDispatcher("errorPage3.html");
		    rd.include(request, response);
		}
		    
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
