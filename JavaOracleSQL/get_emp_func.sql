--------------------------------------------------------
--  File created - Wednesday-May-06-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function GET_EMP_ANNSAL
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYSTEM"."GET_EMP_ANNSAL" (E_EMPNO IN NUMBER)
RETURN NUMBER
IS
ANNUAL_SALARY NUMBER;
BEGIN
SELECT SAL*12 INTO ANNUAL_SALARY FROM EMP WHERE EMPNO=E_EMPNO;
RETURN ANNUAL_SALARY;

EXCEPTION
WHEN NO_DATA_FOUND THEN
RETURN -1;

END;

/
