--------------------------------------------------------
--  File created - Wednesday-May-06-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure DISPLAY_RECORDS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYSTEM"."DISPLAY_RECORDS" (P_JOB IN VARCHAR)
IS
BEGIN
DBMS_OUTPUT.PUT_LINE('EmployeeNumber' || '    ' ||'SALARY'||'    '||'DepartmentNumber' || '    '||'JOB');
FOR EMPRECORD IN (SELECT EMPNO, SAL, DEPTNO, JOB FROM EMP WHERE JOB=P_JOB)
LOOP
DBMS_OUTPUT.PUT_LINE(EMPRECORD.EMPNO ||'             '|| EMPRECORD.SAL||'     ' ||EMPRECORD.DEPTNO || '              '|| EMPRECORD.JOB);
END LOOP;

EXCEPTION
WHEN NO_DATA_FOUND THEN
DBMS_OUTPUT.PUT_LINE('NO EMPLOYEE WITH THE GIVEN JOB EXISTS');

END;

/
