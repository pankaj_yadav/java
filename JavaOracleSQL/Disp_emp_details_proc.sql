--------------------------------------------------------
--  File created - Wednesday-May-06-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure DISP_EMP_DETAILS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYSTEM"."DISP_EMP_DETAILS" (iEMPNO IN NUMBER, sGRADE OUT NUMBER, sSALARY OUT NUMBER)
IS
BEGIN
SELECT S.GRADE, E.SAL INTO sGRADE, sSALARY FROM SALGRADE S, EMP E
WHERE E.SAL BETWEEN S.LOSAL AND S.HISAL AND E.EMPNO=iEMPNO;

EXCEPTION
WHEN NO_DATA_FOUND THEN 
      DBMS_OUTPUT.PUT_LINE('No such Employee!');
WHEN others THEN 
      DBMS_OUTPUT.PUT_LINE('Error!'); 

END;

/
